@extends('layout')

@section('title', "Formulaire d'ajout d'une nouvelle catégorie")

@section('content')
    <h3>Formulaire d'ajout d'une nouvelle catégorie</h3>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('categories.store') }}" method="post">
        @csrf
        @include('categories.form')
        <div class="d-grid gap-2">
            <button class="btn btn-success">
                Ajouter une nouvelle catégorie
            </button>
            <a href="{{ route('categories.index') }}" class="btn btn-secondary">
                Annuler la création de nouvelle catégorie
            </a>
        </div>
    </form>

@endsection